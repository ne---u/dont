# — DON'T

Based on [Webpack Frontend Starterkit](https://github.com/wbkd/webpack-starter)

### Installation

```
npm install
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

### [Demo](https://dont.ne-u.xyz/)