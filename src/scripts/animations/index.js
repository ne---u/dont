import sliderFirst from './timelines/sliderFirst';
import sliderEnter from './timelines/sliderEnter';
import sliderLeave from './timelines/sliderLeave';
import loader from './timelines/loader';

const animations = new Map();

animations.set('slider-first', sliderFirst);
animations.set('slider-enter', sliderEnter);
animations.set('slider-leave', sliderLeave);
animations.set('loader', loader);

export default animations;