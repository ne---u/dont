import anime from 'animejs';

export default function loader(el) {
  const prev = el.querySelector('.item--prev');
  const current = el.querySelector('.item--current');
  const next = el.querySelector('.item--next');

  const title = Array.from(el.querySelectorAll('.prealoder__title'));

  const tl = anime.timeline();

  tl
    .add({
      targets: title,
      clipPath: ['inset(0.001% 0.003% 0.002% 100%)', 'inset(0.001% 0% 0.003% 0.002%)'],
      easing: 'easeOutExpo',
      duration: 1500,
      delay: anime.stagger(200),
      complete: () => {
        document.querySelector('header .title').removeAttribute('style');
      },
    }, 0)
    .add({
      targets: current,
      clipPath: ['inset(0.001% 0.002% 100% 0.003%)', 'inset(0.001% 0.002% 0% 0.003%)'],
      easing: 'easeOutExpo',
      duration: 2000,
    }, 400)
    .add({
      targets: next,
      clipPath: ['inset(0.001% 100% 0.003% 0.002%)', 'inset(0.001% 0% 0.003% 0.002%)'],
      easing: 'easeOutExpo',
      duration: 2000,
    }, 0)
    .add({
      targets: prev,
      clipPath: ['circle(0% at 0% 0%)', 'circle(50% at 50% 50%)'],
      easing: 'easeOutExpo',
      duration: 2000,
    }, 800)
    .add({
      targets: title,
      clipPath: ['inset(0.001% 0% 0.003% 0.002%)', 'inset(0.001% 100% 0.003% 0.002%)'],
      easing: 'easeOutExpo',
      duration: 1200,
      delay: anime.stagger(200, { from: 'last', }),
    })
    .add({
      targets: prev,
      clipPath: ['circle(50% at 50% 50%)', 'circle(0% at 0% 0%)'],
      easing: 'easeOutExpo',
      duration: 2000,
    }, '-=1800')
    .add({
      targets: current,
      clipPath: ['inset(0.001% 0.002% 0% 0.003%)', 'inset(100% 0.002% 0% 0.003%)'],
      easing: 'easeOutCubic',
      duration: 600,
    }, '-=1800')
    .add({
      targets: next,
      clipPath: ['inset(0.001% 0% 0.003% 0.002%)', 'inset(0.001% 0% 0.003% 100%)'],
      easing: 'easeOutCubic',
      duration: 600,
    }, '-=1800')
    .add({
      targets: prev,
      opacity: [1, 0],
      duration: 10,
    }, '-=1800')
    .add({
      targets: [next, current],
      opacity: [1, 0],
      duration: 1000,
      easing: 'easeOutCubic',
    }, '-=1700');

  return tl;
}