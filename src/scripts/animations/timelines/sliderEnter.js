import anime from 'animejs';

export default function sliderEnter(el, mode) {
  const tl = anime.timeline();

  const bg = document.querySelector('.bg');

  const prev = el.querySelector('.slider__viewport .item--prev figure');
  const current = el.querySelector('.slider__viewport .item--current figure');
  const next = el.querySelector('.slider__viewport .item--next figure');

  const bgs = Array.from(el.querySelectorAll('.slider__viewport .figure__bg'));

  const counter = el.querySelector('.slider__viewport .item__counter');

  const title = el.querySelector('.slider__viewport .item__title');

  const titleLines = Array.from(el.querySelectorAll('.slider__viewport .item__title .title__stroke span'));

  const detailsLines = Array.from(document.querySelectorAll('.slider > .slider__details bdi span'));

  const cursor = document.querySelector('.cursor .cursor__circle svg');

  const fx = {};

  if (mode === 'next') {
    fx.current = ['inset(0.001% 0.002% 0% 100.003%)', 'inset(0.001% 0.002% 0% -100.003%)'];
    fx.next = ['inset(0.001% 0.002% 0% 100.003%)', 'inset(0.001% 0.002% 0% -0.003%)'];
    fx.title = ['100%', '0%'];
  } else if (mode === 'prev') {
    fx.current = ['inset(0.001% 100.002% 0% -100.003%)', 'inset(0.001% 0.002% 0% -100.003%)'];
    fx.next = ['inset(0.001% 100.002% 0% -0.003%)', 'inset(0.001% 0.002% 0% -0.003%)'];
    fx.title = ['-100%', '0%'];
  }

  tl
    .add({
      targets: current,
      clipPath: fx.current,
      easing: 'easeOutExpo',
      duration: mode === 'next' ? 4000 : 2000,
    }, 200)
    .add({
      targets: next,
      clipPath: fx.next,
      easing: 'easeOutExpo',
      duration: 2000,
    }, 200)
    .add({
      targets: prev,
      clipPath: ['circle(0% at 50% 100%)', 'circle(50% at 50% 50%)'],
      easing: 'easeOutExpo',
      duration: 2000,
    }, 800)
    .add({
      targets: bgs[1],
      scale: [1.5, 1],
      easing: 'easeOutExpo',
      duration: 2200,
    }, 150)
    .add({
      targets: bgs[2],
      scale: [1.5, 1],
      easing: 'easeOutExpo',
      duration: 1200,
    }, 350)
    .add({
      targets: bgs[0],
      scale: [1.5, 1],
      easing: 'easeOutExpo',
      duration: 1200,
    }, 1000)
    .add({
      targets: bg,
      clipPath: ['inset(0.001% 100% 0.003% 0.002%)', 'inset(0.001% 0% 0.003% 0.002%)'],
      easing: 'easeOutExpo',
      duration: 2200,
    }, 1200)
    .add({
      targets: title.children[1].children,
      clipPath: ['inset(-0.001% 100% -0.003% -0.002%)', 'inset(-0.001% 0% -0.003% -0.002%)'],
      easing: 'easeOutExpo',
      duration: 1000,
      delay: anime.stagger(200),
    }, 1000)
    .add({
      targets: titleLines,
      translateY: fx.title,
      easing: 'easeOutExpo',
      duration: 1000,
      delay: anime.stagger(200),
    }, 1000)
    .add({
      targets: title.children[0].children,
      clipPath: ['inset(0.001% 100% 0.003% 0.002%)', 'inset(0.001% 20% 0.003% 0.002%)'],
      easing: 'easeOutElastic(1, 0.8)',
      duration: 2000,
      delay: anime.stagger(100),
    }, 1600)
    .add({
      targets: detailsLines,
      translateY: ['100%', '0%'],
      opacity: [0, 1],
      easing: 'easeOutExpo',
      duration: 1000,
      delay: anime.stagger(150),
    }, 1400)
    .add({
      targets: counter.children,
      opacity: [0, 1],
      translateY: ['100%', '0%'],
      easing: 'easeOutExpo',
      duration: 800,
      delay: anime.stagger(200, { from: 'last', }),
    }, 1200);

    anime({
      targets: cursor,
      scale: [1, 0.95],
      easing: 'easeOutCirc',
      duration: 400,
      direction: 'alternate',
    });

  return tl;
}