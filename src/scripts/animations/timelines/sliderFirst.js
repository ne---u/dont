import anime from 'animejs';

export default function sliderFirst(el) {
  const tl = anime.timeline();

  const bg = document.querySelector('.bg');

  const prev = el.querySelector('.slider__viewport .item--prev figure');
  const current = el.querySelector('.slider__viewport .item--current figure');
  const next = el.querySelector('.slider__viewport .item--next figure');

  const bgs = Array.from(el.querySelectorAll('.figure__bg'));
  const lines = el.querySelector('.lines__nav');
  const counter = el.querySelector('.item__counter');

  const title = el.querySelector('.item__title');

  const titleLines = Array.from(el.querySelectorAll('.item__title .title__stroke span'));

  const detailsLines = Array.from(el.querySelectorAll('.slider__details bdi span'));

  tl
    .add({
      targets: bg,
      clipPath: ['inset(0.001% 100% 0.003% 0.002%)', 'inset(0.001% 0% 0.003% 0.002%)'],
      easing: 'easeOutExpo',
      duration: 2200,
      begin: () => {
        bg.removeAttribute('style');
      },
    }, 0)
    .add({
      targets: bgs,
      scale: [1.5, 1],
      easing: 'easeOutExpo',
      duration: 2000,
    }, 300)
    .add({
      targets: current.parentNode,
      clipPath: ['inset(0.001% 0.002% 100% -100.003%)', 'inset(0.001% 0.002% 0% -100.003%)'],
      easing: 'easeOutExpo',
      duration: 2200,
      begin: () => {
        current.parentNode.removeAttribute('style');
      },
    }, 200)
    .add({
      targets: next.parentNode,
      clipPath: ['inset(0.001% 100% 0.003% 0.002%)', 'inset(0.001% 0% 0.003% 0.002%)'],
      easing: 'easeOutExpo',
      duration: 1800,
      begin: () => {
        next.parentNode.removeAttribute('style');
      },
    }, 400)
    .add({
      targets: prev.parentNode,
      clipPath: ['circle(0% at 50% 100%)', 'circle(50% at 50% 50%)'],
      easing: 'easeOutExpo',
      duration: 1800,
      begin: () => {
        prev.parentNode.removeAttribute('style');
      },
    }, 700)
    .add({
      targets: lines.children,
      scaleX: [0, 1],
      easing: 'easeOutExpo',
      duration: 800,
      delay: anime.stagger(200, { from: 'center' }),
      begin: () => {
        lines.removeAttribute('style');
      },
    }, 400)
    .add({
      targets: counter.children,
      opacity: [0, 1],
      translateY: ['100%', '0%'],
      easing: 'easeOutExpo',
      duration: 800,
      delay: anime.stagger(200, { from: 'last', }),
      begin: () => {
        counter.removeAttribute('style');
      },
    }, 200)
    .add({
      targets: title.children[1].children,
      clipPath: ['inset(-0.001% 100% -0.003% -0.002%)', 'inset(-0.001% 0% -0.003% -0.002%)'],
      easing: 'easeOutExpo',
      duration: 1600,
      delay: anime.stagger(200),
      begin: () => {
        title.removeAttribute('style');
        title.children[0].style.opacity = 0;
      },
    }, 400)
    .add({
      targets: titleLines,
      translateY: ['100%', '0%'],
      easing: 'easeOutExpo',
      duration: 1000,
      delay: anime.stagger(200),
    }, 300)
    .add({
      targets: title.children[0].children,
      clipPath: ['inset(0.001% 100% 0.003% 0.002%)', 'inset(0.001% 20% 0.003% 0.002%)'],
      easing: 'easeOutElastic(1, 0.8)',
      duration: 3000,
      delay: anime.stagger(100),
      begin: () => {
        title.children[0].removeAttribute('style');
      },
    }, 800)
    .add({
      targets: detailsLines,
      translateY: ['100%', '0%'],
      opacity: [0, 1],
      easing: 'easeOutExpo',
      duration: 1000,
      delay: anime.stagger(150),
      begin: () => {
        document.querySelector('.slider__details').removeAttribute('style');
      },
    }, 1000);

  return tl;
}