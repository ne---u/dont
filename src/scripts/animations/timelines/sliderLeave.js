import anime from 'animejs';

export default function sliderLeave(el, mode) {
  const tl = anime.timeline();

  const bg = document.querySelector('.bg');

  const prev = el.querySelector('.slider__transition .item--prev figure');
  const current = el.querySelector('.slider__transition .item--current figure');
  const next = el.querySelector('.slider__transition .item--next figure');

  const counter = el.querySelector('.slider__transition .item__counter');

  const title = el.querySelector('.slider__transition .item__title');

  const titleLines = Array.from(el.querySelectorAll('.slider__transition .item__title .title__stroke span'));

  const detailsLines = Array.from(el.querySelectorAll('.slider__transition .slider__details bdi span'));

  const fx = {};
  
  if (mode === 'next') {
    fx.current = ['inset(0.001% 0.002% 0% -100.003%)', 'inset(0.001% 100.002% 0% -100.003%)'];
    fx.next = ['inset(0.001% 0.002% 0% 0.003%)', 'inset(0.001% 100.002% 0% 0.003%)'];
    fx.title = ['0%', '105%'];
  } else if (mode === 'prev') {
    fx.current = ['inset(0.001% 0.002% 0% -100.003%)', 'inset(0.001% 0.002% 0% -100.003%)'];
    fx.next = ['inset(0.001% 0.002% 0% 0.003%)', 'inset(0.001% 0.002% 0% 100.003%)'];
    fx.title = ['0%', '-105%'];
  }

  tl
    .add({
      targets: bg,
      clipPath: ['inset(0.001% 0% 0.003% 0.002%)', 'inset(0.001% 100% 0.003% 0.002%)'],
      easing: 'easeOutExpo',
      duration: 1200,
    }, 0)
    .add({
      targets: current,
      clipPath: fx.current,
      easing: 'easeOutExpo',
      duration: mode === 'next' ? 4000 : 3000,
    }, 200)
    .add({
      targets: next,
      clipPath: fx.next,
      easing: 'easeOutExpo',
      duration: 1600,
    }, 200)
    .add({
      targets: prev,
      clipPath: ['circle(50% at 50% 50%)', 'circle(0% at 50% 100%)'],
      easing: 'easeOutExpo',
      duration: 1200,
    }, 0)
    .add({
      targets: next,
      opacity: [1, 0],
      duration: 600,
      easing: 'easeOutExpo',
    }, 600)
    .add({
      targets: title.children[0].children,
      clipPath: ['inset(0.001% 20% 0.003% 0.002%)', 'inset(0.001% 100% 0.003% 0.002%)'],
      easing: 'easeOutExpo',
      duration: 600,
      delay: anime.stagger(100),
    }, 200)
    .add({
      targets: titleLines,
      translateY: fx.title,
      easing: 'easeOutExpo',
      duration: 1200,
      delay: anime.stagger(100),
    }, 400)
    .add({
      targets: detailsLines,
      translateY: ['0%', '100%'],
      opacity: [1, 0],
      easing: 'easeOutExpo',
      duration: 1000,
      delay: anime.stagger(150),
    }, 0)
    .add({
      targets: counter.children,
      opacity: [1, 0],
      translateY: ['0%', '100%'],
      easing: 'easeOutExpo',
      duration: 600,
      delay: anime.stagger(200, { from: 'last', }),
    }, 200);

  return tl;
}