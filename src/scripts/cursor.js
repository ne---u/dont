import Aion from '@adoratorio/aion';

class Cursor {
  constructor(el) {
    this.el = el;
    this.aion = new Aion();
    this.bound = {
      width: 55,
      height: 55,
    };
    this.lerpedCoords = {
      x: (window.innerWidth / 2) - (this.bound.width / 2),
      y: (window.innerHeight / 2) - (this.bound.height / 2),
    };
    this.coords = {
      x: (window.innerWidth / 2) - (this.bound.width / 2),
      y: (window.innerHeight / 2) - (this.bound.height / 2),
    };
    this.prevCoords = {
      x: 0,
      y: 0,
    };

    this.frameHandler = () => this.frame();
    this.mouseHandler = (event) => this.detectCoords();

    document.body.addEventListener('mousemove', this.mouseHandler);

    this.aion.add(this.frameHandler, 'frame');
    this.aion.start();
  }

  detectCoords() {
    this.coords.x = event.clientX - (this.bound.width / 2);
    this.coords.y = event.clientY - (this.bound.height / 2);
  }

  frame() {
    this.lerpedCoords = {
      x: this.lerp(this.coords.x, this.prevCoords.x, 0.85),
      y: this.lerp(this.coords.y, this.prevCoords.y, 0.85),
    };

    this.el.style.transform = `translate3d(${this.lerpedCoords.x}px, ${this.lerpedCoords.y}px, 0)`;

    this.prevCoords = this.lerpedCoords;
  }

  lerp(a, b, n) {
    return ((1 - n) * a) + (n * b);
  }
}

export default Cursor;