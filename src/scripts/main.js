import '../styles/style.scss';
import data from '../assets/data.json';
import Slider from './slider';
import Cursor from './cursor';
import animations from './animations';

let slider = null;
let signal = false;
let tlPrev = null;
let tlNext = null;
let inTrans = true;

document.addEventListener('DOMContentLoaded', init);

function init() {
  preloadImages(data);

  loader();

  initCursor();
}

function preloadImages(data) {
  const wrapper = document.querySelector('.imgs__preload');
  data.forEach((item) => {
    const img = document.createElement('img');
    img.src = `/public/img/${item.id}.webp`;
    img.alt = item.title.raw;

    wrapper.appendChild(img);
  });
}

function initSlider() {
  const el = document.querySelector('.slider');
  slider = new Slider(el, data);

  slider.onEnter = sliderEnter;
  slider.onLeave = sliderLeave;

  sliderFirst();
}

function loader() {
  const preloader = document.querySelector('.preloader');

  const tl = animations.get('loader')(preloader);

  tl.update = (anim) => {
    if (Math.round(anim.progress) === 78 && !signal) {
      signal = true;
      document.body.classList.remove('wait');
      initSlider();

      preloader.remove();
    }
  };
}

function sliderFirst() {
  const slider = document.querySelector('.slider');

  const tl = animations.get('slider-first')(slider);

  tl.update = (anim) => {
    if (Math.round(anim.progress) === 50 && signal) {
      signal = false;

      inTrans = false;
    }
  };

  return tl;
}

function sliderEnter(mode) {
  inTrans = true;

  const slider = document.querySelector('.slider');
  const tl = animations.get('slider-enter')(slider, mode);

  return tl;
}

function sliderLeave(mode) {
  inTrans = true;

  const slider = document.querySelector('.slider');
  const tl = animations.get('slider-leave')(slider, mode);

  return tl;
}

function initCursor() {
  const el = document.querySelector('.cursor');
  const cursor = new Cursor(el);
} 