class Slider {
  constructor(el, data) {
    this.el = el;
    this.data = data;
    this.index = this.data.length - 1;
    this.navigation = {
      prev: this.index,
      current: 0,
      next: 1,
    };

    this.onEnter = () => {};
    this.onLeave = () => {};

    this.signal = false,

    this.dom = {
      viewport: el.querySelector('.slider__viewport'),
      prev: el.querySelector('.item--prev figure'),
      current: el.querySelector('.item--current figure'),
      next: el.querySelector('.item--next figure'),
      title: {
        filler: el.querySelector('.item__title .title__filler'),
        stroke: el.querySelector('.item__title .title__stroke'),
      },
      counter: {
        current: el.querySelector('.counter__current'),
        total: el.querySelector('.counter__total'),
      },
      details: {
        parent: el.querySelector('.slider__details'),
        credits: el.querySelector('.details__credits'),
        date: el.querySelector('.details__date'),
      },
      bg: document.querySelector('.bg'),
      line: {
        nav: el.querySelector('.lines__nav'),
        template: el.querySelector('.line'),
        clone: null,
        items: [],
      },
      hover: el.querySelector('.details__cta .cta__hover'),
      transition: el.querySelector('.slider__transition'),
      cursor: {
        inner: document.querySelector('.cursor .cursor__inner'),
        filler: document.querySelector('.cursor .cursor__circle .filler'),
      },
    };

    this.prevHandler = () => this.prev();
    this.nextHandler = () => this.next();
    this.goToHandler = (event) => this.goTo(event);
    this.keysHandler = (event) => {
      if (event.keyCode === 37) this.prev();
      if (event.keyCode === 39) this.next();
    };

    this.init();
    this.update();
  }

  init() {
    this.dom.transition.style.display = 'none';
    this.dom.counter.total.innerHTML = this.index + 1;

    this.dom.prev.parentNode.addEventListener('click', this.prevHandler);
    this.dom.next.parentNode.addEventListener('click', this.nextHandler);

    document.addEventListener('keydown', this.keysHandler);

    this.setTrack();
    this.updateRef();
    this.updateCursor();
  }

  update() {
    this.dom.prev.querySelector('.figure__bg').style.backgroundImage = `url(/public/img/${this.data[this.navigation.prev].id}.webp)`;
    this.dom.current.querySelector('.figure__bg').style.backgroundImage = `url(/public/img/${this.data[this.navigation.current].id}.webp)`;
    this.dom.next.querySelector('.figure__bg').style.backgroundImage = `url(/public/img/${this.data[this.navigation.next].id}.webp)`;
    
    this.dom.title.filler.innerHTML = this.data[this.navigation.current].title.rendered;
    this.dom.title.stroke.innerHTML = this.dom.title.filler.innerHTML;
    this.dom.details.credits.innerHTML = this.data[this.navigation.current].details.credits.rendered;
    this.dom.details.date.innerHTML = this.data[this.navigation.current].details.date.rendered;
  }

  next() {
    if (this.signal) return;
    this.signal = true;
    this.dom.prev.parentNode.style.pointerEvents = 'none';
    this.dom.next.parentNode.style.pointerEvents = 'none';

    this.removeCurrentTrack();

    this.navigation.prev = this.navigation.current;
    this.navigation.current = this.navigation.current === this.index ? 0 : this.navigation.current + 1;
    this.navigation.next = this.navigation.next === this.index ? 0 : this.navigation.next + 1;

    this.setTransition();
    this.update();

    this.callback('next');
  }

  prev() {
    if (this.signal) return;
    this.signal = true;
    this.dom.prev.parentNode.style.pointerEvents = 'none';
    this.dom.next.parentNode.style.pointerEvents = 'none';

    this.removeCurrentTrack();

    this.navigation.next = this.navigation.current;
    this.navigation.current = this.navigation.current === 0 ? this.index : this.navigation.current - 1;
    this.navigation.prev = this.navigation.current === 0 ? this.index : this.navigation.prev - 1;
    
    this.setTransition();
    this.update();

    this.callback('prev');
  }

  goTo(event) {
    if (this.signal) return;
    this.signal = true;

    const current = parseInt(event.target.dataset.order, 10);
    const direction = current > this.navigation.current ? 'next' : 'prev';

    this.removeCurrentTrack();

    this.navigation.next = current === this.index ? 0 : current + 1;
    this.navigation.current = current;
    this.navigation.prev = current === 0 ? this.index : current - 1;

    this.setTransition();
    this.update();

    this.callback(direction);
  }

  callback(mode) {
    const tlEnter = this.onEnter(mode);
    const tlLeave = this.onLeave(mode);

    this.updateCursor();

    tlLeave.children[0].complete = () => {
      this.updateRef();
    };

    tlLeave.finished.then(() => {
      this.dom.prev.parentNode.style.pointerEvents = 'auto';
      this.dom.next.parentNode.style.pointerEvents = 'auto';
      this.signal = false;
      this.dom.transition.style.display = 'none';
      this.dom.transition.innerHTML = '';
    });

  }

  setTransition() {
    this.dom.transition.style.display = 'block';

    const clones = {};
    clones.prev = this.dom.prev.parentNode.cloneNode(true);
    clones.current = this.dom.current.parentNode.cloneNode(true);
    clones.next = this.dom.next.parentNode.cloneNode(true);
    clones.details = this.dom.details.parent.cloneNode(true);

    clones.prev.dataset.clone = true;
    clones.current.dataset.clone = true;
    clones.next.dataset.clone = true;
    clones.details.dataset.clone = true;

    this.dom.transition.appendChild(clones.prev);
    this.dom.transition.appendChild(clones.current);
    this.dom.transition.appendChild(clones.next);
    this.dom.transition.appendChild(clones.details);
  }

  setTrack() {
    this.dom.line.clone = this.dom.line.template.cloneNode(true);
    this.dom.line.template.remove();

    this.data.forEach((line, index) => {
      const dom = this.dom.line.clone.cloneNode(true);
      dom.style.flexBasis = `${(100 / this.data.length) - 2}%`;
      dom.dataset.order = index;
      const thumb = dom.querySelector('.line__thumb');
      thumb.classList.add(`bg--${line.color}`);

      dom.addEventListener('click', this.goToHandler);

      this.dom.line.nav.append(dom);
      this.dom.line.items.push(dom);
    });
  }

  removeCurrentTrack() {
    this.dom.line.items[this.navigation.current].removeAttribute('data-current');
  }
  
  updateRef() {
    const prevClass = this.dom.bg.classList.item(1);
    this.dom.bg.classList.remove(prevClass);
    this.dom.bg.classList.add(`bg--${this.data[this.navigation.current].color}`);

    this.dom.hover.classList.remove(prevClass);
    this.dom.hover.classList.add(`bg--${this.data[this.navigation.current].color}`);

    this.dom.counter.current.innerHTML = this.navigation.current + 1;
    this.dom.line.items[this.navigation.current].dataset.current = true;
  }

  updateCursor() {
    const prevFillerClass = this.dom.cursor.filler.classList.item(1);
    this.dom.cursor.filler.classList.remove(prevFillerClass);
    this.dom.cursor.filler.classList.add(`filler--${this.data[this.navigation.current].color}`);

    const prevClass = this.dom.cursor.inner.classList.item(1);
    this.dom.cursor.inner.classList.remove(prevClass);
    this.dom.cursor.inner.classList.add(`bg--${this.data[this.navigation.current].color}`);
  }
}

export default Slider;